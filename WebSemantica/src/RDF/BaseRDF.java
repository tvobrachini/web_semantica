package RDF;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.FileManager;
/**
 * Campos e metodos padr�o para manipula��o de RDF
 * @author Luan Fakelmann
 *
 */
public class BaseRDF {
	
	public BaseRDF(){
		modelo = ModelFactory.createDefaultModel();
		//Propriedade SubClassOf
		subClassOf = modelo.getProperty("http://www.w3.org/2000/01/rdf-schema#subClassOf");
	}
	
	/**
	 * 
	 * @return URI da ontologia criada (Academic Social Network Ontology)
	 */
	public String getAsnOnt() {
		return asnOnt;
	}
	
	/**
	 * 
	 * @return URI do vocabulario AIISO (Academic Institution Internal Structure Ontology)
	 */
	public String getAiiso() {
		return aiiso;
	}
	
	/**
	 * 
	 * @return URI do vocabulario FOAF (Friend of a Friend)
	 */
	public String getFoaf() {
		return foaf;
	}
	
	/**
	 * 
	 * @return URI do vocabulario AIISO-ROLES (Academic Institution Internal Structure Ontology - Roles)
	 */
	public String getAiisoRoles() {
		return aiisoRoles;
	}
	
	/**
	 * 
	 * @return Propriedade rdfs:subclassOf
	 */
	public Property getSubClassOf() {
		return subClassOf;
	}
	
	/**
	 * Metodo para obter todos os conceitos utilizados
	 * @return Conceitos utilizados no contexto da Rede Social
	 */
	public Resource[] getTodosConceitos(){
		abrirTodas();
		Resource[] conceitos = 
			   {mdFoaf.getResource("http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing"),
				mdFoaf.getResource(foaf+"Person"),
				mdFoaf.getResource(foaf+"Document"),
				mdAiiso.getResource(aiiso+"Institution"),
				mdAiiso.getResource(aiiso+"College"),
				mdAiiso.getResource(aiiso+"Course"),
				mdAiiso.getResource(aiiso+"Subject"),
				modelo.getResource(aiiso+"KnowledgeGrouping"),
				modelo.getResource(asnOnt+"Student"),
				mdAiisoRoles.getResource(aiisoRoles+"Professor"),
				mdAiisoRoles.getResource(aiisoRoles+"Research"),
				mdAiisoRoles.getResource(aiisoRoles+"Researcher"),
				mdAiisoRoles.getResource(aiisoRoles+"Research_Leader"),
				mdAiisoRoles.getResource(aiisoRoles+"Research_Student"),
				mdFoaf.getResource(foaf+"Agent"),
				mdFoaf.getResource(foaf+"Organization")};
		/*EXIBICAO DOS CONCEITOS
		for(Resource con : conceitos){
			System.out.println(con);
		}
		*/
		return conceitos;		
	}
	
	/**
	 * Metodo para obter as ObjetProperties utilizadas
	 * @return ObjectProperties utilizadas no contexto da Rede Social
	 */
	public Property[] getObjectProperties(){
		List<Property> propriedades = new ArrayList<Property>();
		Property[] pro;
		//Listando propriedades ASNO
		abrirAsnOnt();
		Property p, range = modelo.getProperty("http://www.w3.org/2000/01/rdf-schema#range");
		Resource str = modelo.getResource("http://www.w3.org/2001/XMLSchema#string");
		Iterator<Resource> it = modelo.listSubjectsWithProperty(range);
		while(it.hasNext()){
			Resource res = modelo.getResource(it.next().toString());
			if(!res.hasProperty(range, str)){
				p = modelo.getProperty(res.toString());
				propriedades.add(p);				
			}
		}
		//Listando propriedades FOAF utilizadas
		abrirFoaf();
		String[] propFoaf = {"publications",
							"primaryTopic",
							"topic",
							//"knows",
							"currentProject",
							"pastProject"};
		
		for(String prop : propFoaf){
			p = mdFoaf.getProperty(foaf + prop);
			propriedades.add(p);
		}
		//Add propriedade aiiso
		abrirAiiso();
		String[] propAiiso ={"part_of"};
							//"knowledgeGrouping"};
		
		for(String prop : propAiiso){
			p = mdAiiso.getProperty(aiiso + prop);
			propriedades.add(p);
		}	
		pro = new Property[propriedades.size()];
		//Passando para para o vetor
		int i = 0;
		for(Property pr : propriedades){			
			pro[i] = pr;
			//EXIBICAO DAS PROPRIEDADES
			//System.out.println(pro[i]);
			i++;
		}		
		return pro;
	}
	
	/**
	 * Metodo para retornar o conceito Domain de uma propriedade
	 * @param propriedade - propriedade a ser analisada
	 * @return Domain da propriedade
	 */
	public Resource getDomain(Property propriedade){
		Model base = propriedade.getModel();
		Resource prop = propriedade.asResource();
		prop =  prop.getPropertyResourceValue(base.getProperty("http://www.w3.org/2000/01/rdf-schema#domain"));
		if (prop.getLocalName() == null)
			prop = getRange(propriedade);
		return prop;
	}
	
	/**
	 * Metodo para retornar o conceito Range de uma propriedade
	 * @param propriedade - propriedade a ser analisada
	 * @return Range da propriedade
	 */
	public Resource getRange(Property propriedade){
		Model base = propriedade.getModel();
		Resource prop = propriedade.asResource();
		prop = prop.getPropertyResourceValue(base.getProperty("http://www.w3.org/2000/01/rdf-schema#range"));
		if (prop.getLocalName() == null)
			prop = getDomain(propriedade);
		return prop;
	}		
	
	/**
	 * Metodo para ler a ontologia ASNONT (Academic Social Network Ontology)
	 */
	protected void abrirAsnOnt(){	
		//Abrindo arquivo em disco
		InputStream in = FileManager.get().open("C:\\ontologies\\asno.owl");
		if(in==null)
			throw new IllegalArgumentException("Arquivo nao encontrado!");
		else{
			if(!asnOntOk){				
				System.out.println("arquivo ok!");		
				//leitura do arquivo
				modelo = ModelFactory.createDefaultModel();
				modelo.read(in,null);
				System.out.println("asnOnt carregada!");
				asnOntOk = true;
			}
		}
	}
	
	/**
	 * Metodo para ler o vocabulario FOAF
	 */
	protected void abrirFoaf(){
		if(!foafOk){
			mdFoaf = ModelFactory.createDefaultModel();
			mdFoaf.read(foaf);
			System.out.println("foaf carregada");
			foafOk = true;
		}
	}

	/**
	 * Metodo para ler o vocabulario AIISO
	 */
	protected void abrirAiiso(){	
		if(!aiisoOk){
			mdAiiso =  ModelFactory.createDefaultModel();
			mdAiiso.read(aiiso);
			System.out.println("aiiso carregada");
			aiisoOk = true;
		}
	}
	
	/**
	 * Metodo para ler o vocabulario AIISO-ROLES
	 */
	protected void abrirAiisoRoles(){
		if(!aiisoRolesOk){
			mdAiisoRoles = ModelFactory.createDefaultModel();
			mdAiisoRoles.read(aiisoRoles);
			System.out.println("aiiso-roles carregada");
			aiisoRolesOk = true;
		}
		
	}
	
	/**
	 * Metodo para ler todos os vocabularios
	 */
	protected void abrirTodas(){
		abrirAsnOnt();
		abrirFoaf();
		abrirAiiso();
		abrirAiisoRoles();
	}
	
	
	/**
	 * Metodo para corrigir os NameSpaces ao gerar o RDF
	 */
	protected void corrigirNs(){
		modelo.setNsPrefix("aiiso",aiiso).setNsPrefix("asnOnt", asnOnt);
	}
	
	protected String asnOnt = "http://www.ifsp.edu.br/AcademicSocialNetworkOntology.owl#";
	protected String url = "http://www.ifsp.edu.br/";
	protected String aiiso = "http://purl.org/vocab/aiiso/schema#";
	protected String foaf = "http://xmlns.com/foaf/0.1/";
	protected String aiisoRoles = "http://purl.org/vocab/aiiso-roles/schema#";
	protected String dir = "/home/luan/RDFS";
	protected static Model modelo, mdAiiso, mdAiisoRoles, mdFoaf;
	protected static boolean asnOntOk, aiisoOk, aiisoRolesOk, foafOk;
	protected Property subClassOf;

}
