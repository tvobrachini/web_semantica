package RDF;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ModelMaker;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;


public class CollegeRDF extends BaseRDF{
	private Resource College, Institution;
	private Resource[] Course;
	private Property city, state, hasCourse, partOf;
	
	String nome = "College08";
	String instituicao = "Institution03";
	String local = "Sao Paulo";
	int numCour = 4;
	String[] cour= {"Course02",
					"Course03",
					"Course04",
					"Course05"};
	
	//Construtor da Classe
	public CollegeRDF()
	{
		
		//leitura do arquivo
		abrirAsnOnt();
		
		//Recuperando recursos e propriedades
		College = modelo.getResource("http://purl.org/vocab/aiiso/schema#College");
		Institution = modelo.getResource(url + instituicao);
		Course = new Resource[numCour];
		for(int i=0; i< numCour; i++){
			Course[i] = modelo.getProperty(url + cour[i]);
		}
		partOf = modelo.getProperty(aiiso, "part_of");
		city = modelo.getProperty(asnOnt, "city");
		state = modelo.getProperty(asnOnt, "state");
		hasCourse = modelo.getProperty(asnOnt,"hasCourse");
		
	}

	//Metodo para gerar o RDF do campus cadastrado
public void gerarRdf(){			
		
		ModelMaker maker = ModelFactory.createFileModelMaker(dir);
		modelo = maker.createModel(nome + ".rdf",false);
		//Formando o URI do campus
		String uri = url + nome;
		//Criacao do Recurso
		modelo.begin();
		//Corrigindo NameSpaces
		corrigirNs();
		Resource camp = modelo.createResource(uri);		
		camp.addProperty(RDF.type, College)
					.addProperty(city, local)
					.addProperty(state, "SP")
					.addProperty(partOf,Institution);
		for(int i=0; i<numCour; i++){
			camp.addProperty(hasCourse, Course[i]);
		}
		modelo.commit();
		modelo.write(System.out);
		
	}
}
