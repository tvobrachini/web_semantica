package RDF;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ModelMaker;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;


public class CourseRDF extends BaseRDF {
	 
	private Resource Course;
	private Resource[] subject;
	private Property courseAcronym, courseName, hasSubject, inKnowledgeGrouping;
	//NOME PARA ARQUIVO E URI
	String nome = "Course05";
	String codigo = "ODO";
	String descricao = "Odontologia";
	String kg = asnOnt + "KnowledgeGropuing02";
	int numDis = 5;
	String[] dis = {"Subject01",
					"Subject02",
					"Subject06",
					"Subject12",
					"Subject13"};
	
	
	//Construtor da Classe
	public CourseRDF()
	{		
		//Abrindo arquivo
		abrirAsnOnt();
		
		//Recuperando recursos e propriedades
		Course = modelo.getResource("http://purl.org/vocab/aiiso/schema#Course");
		subject = new Resource[numDis];
		for(int i=0; i<numDis; i++){
			subject[i] = modelo.getResource(url + dis[i]);
		}
		courseName = modelo.getProperty(asnOnt, "courseName");
		courseAcronym = modelo.getProperty(asnOnt, "courseAcronym");
		hasSubject = modelo.getProperty(asnOnt, "hasSubject");
		inKnowledgeGrouping = modelo.getProperty(asnOnt, "inKnowledgeGrouping");

	}

	//Metodo para gerar o RDF do campus cadastrado
	public void gerarRdf(){			
		
		ModelMaker maker = ModelFactory.createFileModelMaker(dir);
		modelo = maker.createModel(nome + ".rdf",false);
		//Formando o URI do campus
		String uri = asnOnt + nome;
		//Criacao do Recurso
		modelo.begin();
		//Corrigindo NameSpaces
		corrigirNs();
		Resource camp = modelo.createResource(uri);		
		camp.addProperty(RDF.type, Course)
					.addProperty(courseAcronym, codigo)
					.addProperty(courseName, descricao)
					.addProperty(inKnowledgeGrouping, kg);
		for(int i=0; i<numDis; i++){
			camp.addProperty(hasSubject, subject[i]);
		}
		modelo.commit();
		modelo.write(System.out);
		
	}
}
