package RDF;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ModelMaker;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;


public class InstitutionRDF extends BaseRDF {
	 
	private Resource Institution;
	private Property institutionAcronym, institutionName;
	//NOME PARA ARQUIVO E URI
	String nome = "Institution02";
	String codigo = "UniY";
	String descricao = "Universidade Y";
	
	//Construtor da Classe
	public InstitutionRDF()
	{		
		//Abrindo arquivo
		abrirAsnOnt();
		
		//Recuperando recursos e propriedades
		Institution = modelo.getResource("http://purl.org/vocab/aiiso/schema#Institution");
		institutionName = modelo.getProperty(asnOnt, "institutionName");
		institutionAcronym = modelo.getProperty(asnOnt, "institutionAcronym");
	}

	//Metodo para gerar o RDF do campus cadastrado
	public void gerarRdf(){			
		
		ModelMaker maker = ModelFactory.createFileModelMaker(dir);
		modelo = maker.createModel(nome + ".rdf",false);
		//Formando o URI do campus
		String uri = url + nome;
		//Criacao do Recurso
		modelo.begin();
		//Corrigindo NameSpaces
		corrigirNs();
		Resource camp = modelo.createResource(uri);		
		camp.addProperty(RDF.type, Institution)
					.addProperty(institutionAcronym, codigo)
					.addProperty(institutionName, descricao);

		modelo.commit();
		modelo.write(System.out);
		
	}
}
