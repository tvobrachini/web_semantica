package RDF;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ModelMaker;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;


public class KnowledgeGroupingRDF extends BaseRDF {
	 
	private Resource KnowledgeGrouping, kg;
	private Property code, description, knowledgeGrouping;
	//NOME PARA ARQUIVO E URI
	String nome = "KnowledgeGrouping03";
	//DADOS
	String codigo = "A03";
	String descricao = "Teoria Geral do Direito";
	String area = asnOnt + "KnowledgeGrouping13";
	
	//Construtor da Classe
	public KnowledgeGroupingRDF()
	{		
		//Abrindo arquivo
		abrirAsnOnt();
		
		//Recuperando recursos e propriedades
		KnowledgeGrouping = modelo.getResource("http://purl.org/vocab/aiiso/schema#KnowledgeGrouping");
		kg = modelo.getResource(url + area);
		code = modelo.getProperty(aiiso, "code");
		description = modelo.getProperty(aiiso, "description");
		knowledgeGrouping = modelo.getProperty(aiiso,"knowledgeGrouping");

	}

	//Metodo para gerar o RDF do campus cadastrado
	public void gerarRdf(){			
		
		ModelMaker maker = ModelFactory.createFileModelMaker(dir);
		modelo = maker.createModel(nome + ".rdf",false);
		//Formando o URI do campus
		String uri = url + nome;
		//Criacao do Recurso
		modelo.begin();
		//Corrigindo NameSpaces
		corrigirNs();
		
		Resource camp = modelo.createResource(uri);		
		camp.addProperty(RDF.type, KnowledgeGrouping)
					.addProperty(code,codigo)
					.addProperty(description,descricao)
					.addProperty(knowledgeGrouping,kg);
		modelo.commit();
		modelo.write(System.out);
		
	}
}
