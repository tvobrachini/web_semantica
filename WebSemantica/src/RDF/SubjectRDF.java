package RDF;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ModelMaker;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;


public class SubjectRDF extends BaseRDF {
	 
	private Resource Subject;
	private Property subjectAcronym, subjectName;
	//NOME PARA ARQUIVO E URI
	String nome = "Subject13";
	String codigo = "DIK";
	String descricao = "Disciplina_K";

	
	//Construtor da Classe
	public SubjectRDF()
	{		
		//Abrindo arquivo
		abrirAsnOnt();
		
		//Recuperando recursos e propriedades
		Subject = modelo.getResource("http://purl.org/vocab/aiiso/schema#Subject");
		subjectName = modelo.getProperty(asnOnt, "subjectName");
		subjectAcronym = modelo.getProperty(asnOnt, "subjectAcronym");

	}

	//Metodo para gerar o RDF do campus cadastrado
	public void gerarRdf(){			
		
		ModelMaker maker = ModelFactory.createFileModelMaker(dir);
		//OntModelSpec spec = new OntModelSpec(OntModelSpec.RDFS_MEM);
		modelo = maker.createModel(nome + ".rdf",false);
		//OntModel base = ModelFactory.createOntologyModel(spec,modelo);
		//Formando o URI do campus
		String uri = asnOnt + nome;
		//Criacao do Recurso
		modelo.begin();
		//Corrigindo NameSpaces
		corrigirNs();
		Resource camp = modelo.createResource(uri);		
		camp.addProperty(RDF.type, Subject)
					.addProperty(subjectAcronym, codigo)
					.addProperty(subjectName, descricao);
		modelo.commit();
		modelo.write(System.out);
		
	}
}
