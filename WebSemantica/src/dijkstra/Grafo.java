package dijkstra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

import RDF.BaseRDF;

import dijkstra.No;
/**
 * Implementa��o do algoritmo Dijkstra aplicado ao contexto da Web Sem�ntica
 * @author ??? - adaptado por Luan Fakelmann
 *
 */
public class Grafo extends BaseRDF{
	private List<Aresta> arestas = new ArrayList<Aresta>();
	private List<No> nos = new ArrayList<No>();
	private No[] N;
	private List<No[]> distanciasRoot;
	private int profundidade = 0;
	public static int INFINITY = Integer.MAX_VALUE;
	
	private int Droot=1, Dpeople=0, Duniversity=0, Dknowledge=0, Dresearch=0, Ddocument=0 ;
	
	
	public List<No[]> getDistanciasRoot() {
		return distanciasRoot;
	}

	public void setDistanciasRoot(List<No[]> distanciasRoot) {
		this.distanciasRoot = distanciasRoot;
	}

	
	public int getDroot() {
		return Droot;
	}

	public int getDpeople() {
		return Dpeople;
	}

	public int getDuniversity() {
		return Duniversity;
	}

	public int getDknowledge() {
		return Dknowledge;
	}

	public int getDresearch() {
		return Dresearch;
	}

	public int getDdocument() {
		return Ddocument;
	}

	public Grafo(){
	}
	
	public List<No> getNos(){
		return this.nos;
	}
	
	private boolean existe(List<No> collection, No anItem) {
		return collection.indexOf(anItem) != -1;
	}
	private boolean existe(List<Aresta> collection, Aresta anItem) {
		return collection.indexOf(anItem) != -1;
	}
	
	
	private boolean adicionar(List<No> collection, No obj) {
		if (!existe(collection, obj)) {
			collection.add(obj);			
			return true;
		}		
		// No ja existe
		return false;
	}
	private boolean adicionar(List<Aresta> collection, Aresta obj) {
		if (!existe(collection, obj)) {
			collection.add(obj);			
			return true;
		}		
		// No ja existe
		return false;
	}
	
	private No getNode(String uri){
		for(int i = 0; i<nos.size();i++){
			if(nos.get(i).getConceito().toString().equals(uri)){
				return nos.get(i);				
			}			
		}
		return null;
	}
	
	//Metodo para adcionar todos os nos da ontologia
	public void carregarNos(){
		Resource[] conceitos = getTodosConceitos();
		for(Resource conceito : conceitos){
			String cluster= "root";
			if((conceito.getLocalName()).equals("Agent") ||
					(conceito.getLocalName()).equals("Person") ||
					(conceito.getLocalName()).equals("Student") ||
					(conceito.getLocalName()).equals("Professor") ||
					(conceito.getLocalName()).equals("Researcher") ||
					(conceito.getLocalName()).equals("Research_Leader") ||
					(conceito.getLocalName()).equals("Research_Student")){
				cluster = "people";						
			}
			else if((conceito.getLocalName()).equals("Organization") ||
					(conceito.getLocalName()).equals("Institution") ||
					(conceito.getLocalName()).equals("College")){
				cluster = "university";						
			}
			else if((conceito.getLocalName()).equals("KnowledgeGrouping") ||
					(conceito.getLocalName()).equals("Course") ||
					(conceito.getLocalName()).equals("College") ||
					(conceito.getLocalName()).equals("Subject")){
				cluster = "knowledge";
			}
			else if((conceito.getLocalName()).equals("Research")){
				cluster = "research";
			}
			else if((conceito.getLocalName()).equals("Document")){
				cluster = "document";
			}			
			//System.out.println(cluster);
			No no = new No(conceito, cluster);
			adicionar(this.nos, no);	
		}
	}
	public void carregarNos(Resource[] conceitos){
		for(Resource conceito : conceitos){
			String cluster= "root";
			if((conceito.getLocalName()).equals("Agent") ||
					(conceito.getLocalName()).equals("Person") ||
					(conceito.getLocalName()).equals("Student") ||
					(conceito.getLocalName()).equals("Professor") ||
					(conceito.getLocalName()).equals("Researcher") ||
					(conceito.getLocalName()).equals("Research_Leader") ||
					(conceito.getLocalName()).equals("Research_Student")){
				cluster = "people";						
			}
			else if((conceito.getLocalName()).equals("Organization") ||
					(conceito.getLocalName()).equals("Institution") ||
					(conceito.getLocalName()).equals("College")){
				cluster = "university";						
			}
			else if((conceito.getLocalName()).equals("KnowledgeGrouping") ||
					(conceito.getLocalName()).equals("Course") ||
					(conceito.getLocalName()).equals("College") ||
					(conceito.getLocalName()).equals("Subject")){
				cluster = "knowledge";
			}
			else if((conceito.getLocalName()).equals("Research")){
				cluster = "research";
			}
			else if((conceito.getLocalName()).equals("Document")){
				cluster = "document";
			}			
			//System.out.println(conceito.getLocalName() + " - " + cluster);
			No no = new No(conceito, cluster);
			adicionar(this.nos, no);			
		}
		System.out.println();
	}
	
	//Metodo para adicionar as arestas(propriedades) desejadas
	public void carregarArestas(Property propriedade){
		arestas = new ArrayList<Aresta>();
		//Property[] propriedades = getObjectProperties();
		Resource[] conceitos = getTodosConceitos();
		No no1 = null;
		No no2 = null;		
		
		//Buscando propriedade nos conceitos
		for(Resource con: conceitos){
			Model base = con.getModel();		
			if(con.toString().equals("http://xmlns.com/foaf/0.1/Agent") || con.toString().equals("http://xmlns.com/foaf/0.1/Document") || con.toString().equals("http://purl.org/vocab/aiiso-roles/schema#Research")){
				no1 = getNode(con.toString());
				no2 = getNode("http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing");
			}	
			
			
			if(con.hasProperty(propriedade)){
				//  PS.: Todos conceitos da aiiso-roles sao
				//       subclasses do conceito 'Role'
				if(con.getPropertyResourceValue(propriedade).getLocalName().equals("Role")){
					base = modelo;
				}				
				
				no1 = getNode(con.toString());
				NodeIterator objeto = base.listObjectsOfProperty(con, propriedade);
				//Obtendo superClass
				while(objeto.hasNext()){
					Resource aux = (Resource) objeto.next();
					
					//Validando SuperClass
					if(!aux.toString().equals("http://www.w3.org/2000/10/swap/pim/contact#Person")){						
						no2 = getNode(aux.toString());						
						//
						//
						//CORRIGIR ESTE TRECHO
						//funfando, no entanto eh gambiarra...
						//
						if(con.toString().equals("http://purl.org/vocab/aiiso/schema#College")){
							no2 = getNode("http://purl.org/vocab/aiiso/schema#Institution");
						}
						//System.out.printf("\n" + con +" " + propriedade.getLocalName() + ":  " + no2.getConceito());						
					}
				}						
			}
			Aresta aresta = new Aresta(no1,no2,1);
			if(no1!=null && no2!=null )
				if(!no1.equals(no2))
					adicionar(arestas, aresta);
		}
		
	}
	

	/*
	//Metodo para adicionar todas as arestas(propriedades)
	public void carregarArestas(){
		Property[] propriedades = getObjectProperties();
		Resource[] conceitos = getTodosConceitos();
		Property propriedade = subClassOf;
		No no1 = null;
		No no2 = null;		
		
		//Buscando subclassOf nos conceitos
		for(Resource con: conceitos){
			Model base = con.getModel();		
			if(con.toString().equals("http://xmlns.com/foaf/0.1/Agent") || con.toString().equals("http://xmlns.com/foaf/0.1/Document") || con.toString().equals("http://purl.org/vocab/aiiso-roles/schema#Research")){
				no1 = getNode(con.toString());
				no2 = getNode("http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing");
			}	
			
			
			if(con.hasProperty(propriedade)){
				//  PS.: Todos conceitos da aiiso-roles sao
				//       subclasses do conceito 'Role'
				if(con.getPropertyResourceValue(propriedade).getLocalName().equals("Role")){
					base = modelo;
				}				
				
				no1 = getNode(con.toString());
				NodeIterator objeto = base.listObjectsOfProperty(con, propriedade);
				//Obtendo superClass
				while(objeto.hasNext()){
					Resource aux = (Resource) objeto.next();
					
					//Validando SuperClass
					if(!aux.toString().equals("http://www.w3.org/2000/10/swap/pim/contact#Person")){						
						no2 = getNode(aux.toString());						
						//
						//
						//CORRIGIR ESTE TRECHO
						//funfando, no entanto eh gambiarra...
						//
						if(con.toString().equals("http://purl.org/vocab/aiiso/schema#College")){
							no2 = getNode("http://purl.org/vocab/aiiso/schema#Institution");
						}
						//System.out.printf("\n" + con +" " + propriedade.getLocalName() + ":  " + no2.getConceito());						
					}
				}						
			}
			Aresta aresta = new Aresta(no1,no2,1);
			if(no1!=null && no2!=null )
				if(!no1.equals(no2))
					adicionar(arestas, aresta);
		}
		
		//Criando demais arestas
		for(Property prop : propriedades){
			Resource con1 = getDomain(prop);
			Resource con2 = getRange(prop);
			//System.out.println("\t Domain = "+con1.getLocalName()+" |\t Range = "+con2.getLocalName()+" ->"+prop.getLocalName());
			no1 = getNode(con1.toString());
			no2 = getNode(con2.toString());
			Aresta aresta = new Aresta(no1,no2,1);
			if(no1!=null && no2!=null )
				if(!no1.equals(no2))
					adicionar(arestas, aresta);
			//System.out.println(aresta.getOrigem()+" - "+aresta.getDestino());
		}
		//for(Aresta ar : arestas){
			//System.out.println(ar.getOrigem()+" - "+ar.getDestino());
		//}
		 
	}*/
	
	// Verifica se um determinado n� est� em N
	public boolean emN(No no) {
		for (int i = 0; i < N.length; i++) {
			if (no.compareTo(N[i]) == 0)
				return true;
		}		
		return false;
	}
	
	//Metodo para calcular o menor caminho
	public No[] menorCaminho(Resource conceitoOrigem, Resource conceitoDestino) {
		int i = 0, j = 0;
		No menor = null, atual = null; // �ltimo n� adicionado a N
		No origem = getNode(conceitoOrigem.toString());
		No destino = getNode(conceitoDestino.toString());			
		
		// Inicializa��o
		N = new No[nos.size()];
		N[j++] = origem;
		
		for (No no : nos){
			no.setD(INFINITY);
			no.setAnterior(null);			
		}
		
		atual = origem;
		
		// Dist�ncia de um n� para ele mesmo � inexistente
		atual.setD(0); 
		
		No[] vizinhos = atual.getVizinhos();
		
		if (vizinhos == null)
			return null;
		
		for (i = 0; i < vizinhos.length; i++)
			vizinhos[i].setAnterior(atual);
		
		while (atual.compareTo(destino) != 0) {
			menor = null;
			
			for (No no : nos){
				if (!emN(no)) {
					if (menor == null)
						menor = no;
					
					if (atual.getD() + atual.getPeso(no) < no.getD()) {
						if (atual.getPeso(no) < INFINITY) {
							no.setD(atual.getD() + atual.getPeso(no));
							
							if (atual.getPeso(no) < INFINITY)
								no.setAnterior(atual);
						}
					}
					
					if (no.getD() < menor.getD())
						menor = no;
				}
			}
			
			atual = menor;
			N[j++] = atual;
		}
		
		List<No> caminho = new LinkedList<No>();
		atual = N[j - 1];
		i = 0;
		while (atual != null) {
			caminho.add(atual);
			atual = atual.getAnterior();
		}		
		
		No[]retorno = new No[caminho.size()];
		
		for (i = 0; i < caminho.size(); i++){
			retorno[i] = caminho.get(caminho.size() -1 -i);			
		}		
		
		this.profundidade = retorno.length - 1;
		return retorno;
		
	}
	
	/**
	 * Metodo para calcular a distancia entre os conceitos 
	 * @param conceitos - Vetor contendo os conceitos a serem utilizados
	 * @return
	 */
	public int[][] calcularDistancias(Resource[] conceitos){
		int size = conceitos.length;
		int i,j;
		int[][]distancias = new int[size][size];
		carregarNos(conceitos);
		carregarArestas(subClassOf);
		
		for(i = 0; i< size; i++){
			Resource origem = conceitos[i];
			for(j=0; j<size; j++){
				Resource destino = conceitos[j];
				menorCaminho(origem,destino);
				distancias[i][j] = profundidade;		
			}
		}
		return distancias;
	}
	
	/**
	 * 
	 * @param conceitos - Vetor contendo os conceitos a serem utilizados
	 * @param propriedade - propriedade a ser analisada como aresta
	 * @return List contendo o menor caminho entre os conceitos e a raiz
	 */
	public List<No[]> menorCaminhoRaiz(Resource[] conceitos, Property propriedade){		
		No[] cam = null;
		List<No[]> caminhos = new ArrayList<No[]>();
		int i, raiz=0; 
		boolean thing = false;	
		
		carregarNos(conceitos);
		carregarArestas(subClassOf);
		
		//Verificando se o conceito eh a raiz
		while(!thing){
			if(conceitos[raiz].getLocalName().equals("SpatialThing")){
				thing = true;
			}
			else 
				raiz++;
		}	
		//Caminhos ate raiz		
		for(i=0; i<conceitos.length; i++){
			cam = menorCaminho(conceitos[i], conceitos[raiz]);
			caminhos.add(cam);
		}	
		this.distanciasRoot = caminhos;
		return caminhos;
	}
	
	public void distanciasClusters(){
		for(No[] path : this.distanciasRoot){
			System.out.println(Arrays.deepToString(path));
			if (path[0].getCluster().equals("people") && path.length> this.Dpeople)
				this.Dpeople = path.length;			
			else if (path[0].getCluster().equals("university") && path.length> this.Duniversity)
				this.Duniversity = path.length;	
			else if (path[0].getCluster().equals("knowledge") && path.length> this.Dknowledge)
				this.Dknowledge = path.length;
			else if (path[0].getCluster().equals("research") && path.length> this.Dresearch)
				this.Dresearch = path.length;
			else if (path[0].getCluster().equals("document") && path.length> this.Ddocument)
				this.Ddocument = path.length;
		}
	}
}
