package main;

import semantica.Similaridade;
import semantica.VetorConceitual;

import com.hp.hpl.jena.rdf.model.Resource;

import dijkstra.Grafo;

import RDF.BaseRDF;

public class MainClass {

	public static void main(String[] args) {
		BaseRDF base = new BaseRDF();
		Grafo gr = new Grafo();
		VetorConceitual vc = new VetorConceitual();		
		int distancias[][];
		float normalizado[][];
		float simDis[][];
		float simCos[][];
		float simEsc[][];
		float simProf[][];
		float simCSpec[][];
		int i = 0;
		int j = 0;

		//Carregando Nos(conceitos) e Arestas(propriedades) do grafo		
		Resource[] conceitos = base.getTodosConceitos();
		Similaridade si = new Similaridade(gr, conceitos);	
		int size = conceitos.length;
		
		System.out.printf("\n\n---CONCEITOS---");
		for(Resource con : conceitos){
			System.out.printf("\nC%02d - %s",(i+1),con.getLocalName());
			i++;
		}
		
		System.out.printf("\n\n---DISTANCIAS---");
		//Calculo das Distancias
		distancias = gr.calcularDistancias(conceitos);	
		
		for(i = 0; i< size; i++){			
			System.out.printf("\nC%02d = {", (i+1));			
			for(j=0; j<size; j++){
				System.out.printf("%d, ",distancias[i][j]);				
			}
			System.out.print("}");
		}
		
		//Normalizacao
		normalizado = vc.normalizarVetor(distancias);
		
		System.out.printf("\n\n---VETORES NORMALIZADOS---");
		//Exibindo vetores normalizados
		for(i=0; i<size; i++){
			System.out.printf("\nC%02d = {",(i+1));
			for(j=0; j<size; j++){
				System.out.printf("%.4f, ", normalizado[i][j]);				
			}
			System.out.print("}");
		}

		System.out.printf("\n\n-----------------------------");
		System.out.printf("\n\n---METODOS DE SIMILARIDADE---");
		System.out.printf("\n\n-----------------------------");
		
		System.out.printf("\n\n---SIMILARIDADE - MENOR CAMINHO ENTRE CONCEITOS---");
		//Similaridade por distancia
		simDis = si.similaridadeMenorCaminho(normalizado);
		for(i=0; i<size; i++){
			System.out.printf("\nC%02d = |",(i+1));
			for(j=0; j<size; j++){
				System.out.printf("%.4f |", simDis[i][j]);				
			}
		}
		
		System.out.printf("\n\n---SIMILARIDADE - COSSENO ENTRE VETORES---");
		//Similaridade atraves do cosseno
		simCos = si.similaridadeCos(normalizado);
		for(i=0; i<size; i++){
			System.out.printf("\nC%02d = |",(i+1));
			for(j=0; j<size; j++){
				System.out.printf("%.4f |",simCos[i][j]);
			}
		}
		
		
		
		System.out.printf("\n\n---SIMILARIDADE - MENOR CAMINHO ESCALADO---");
		//Similaridade por metodo Menor Caminho escalado
		simEsc = si.similaridadeMenorCamEsc(distancias);
		for(i=0; i<size; i++){
			System.out.printf("\nC%02d = |",(i+1));
			for(j=0; j<size; j++){
				System.out.printf("%.4f |",simEsc[i][j]);
			}
		}
		
		
		//Similaridade por metodo de profundidade
		simProf = si.similaridadeProfundidade(base.getSubClassOf());
		System.out.printf("\n\n---SIMILARIDADE - PROFUNDIDADE---");
		for(i=0; i<size; i++){
			System.out.printf("\nC%02d = |",(i+1));
			for(j=0; j<size; j++){
				System.out.printf("%.4f |",simProf[i][j]);
			}
		}
		
		//Similaridade por metodo de Menor caminho e especificidade Comum
		simCSpec = si.similaridadeCSpec(distancias, base.getSubClassOf());
		System.out.printf("\n\n---SIMILARIDADE - MENOR CAMINHO E ESPECIFICIDADE COMUM---");
		for(i=0; i<size; i++){
			System.out.printf("\nC%02d = |",(i+1));
			for(j=0; j<size; j++){
				System.out.printf("%.4f |",simCSpec[i][j]);
			}
		}
		
		gr.distanciasClusters();
		System.out.println(gr.getDdocument());
		System.out.println(gr.getDknowledge());
		System.out.println(gr.getDpeople());
		System.out.println(gr.getDresearch());
		System.out.println(gr.getDroot());
		System.out.println(gr.getDuniversity());
		
		float correlacao = si.correlacao(simDis, simCos);
		System.out.printf("\n\nCorrelacao entre 'Menor Caminho' e 'Cosseno dos Vetores de Distancias': %.4f", correlacao);
		 
		correlacao = si.correlacao(simCos, simEsc);
		System.out.printf("\n\nCorrelacao entre 'Cosseno dos Vetores de Distancias' e 'Menor Caminho Escalado':%.4f", correlacao);
		
		correlacao = si.correlacao(simDis, simEsc);
		System.out.printf("\n\nCorrelacao entre 'Menor Caminho' e 'Menor Caminho Escalado':%.4f", correlacao);
		
		correlacao = si.correlacao(simDis, simProf);
		System.out.printf("\n\nCorrelacao entre 'Menor Caminho' e 'Profundidade':%.4f", correlacao);
		
		correlacao = si.correlacao(simCos, simCSpec);
		System.out.printf("\n\nCorrelacao entre 'Cosseno dos Vetores de Distancias' e 'Menor Caminho e Especificidade comum':%.4f", correlacao);
	}

}
