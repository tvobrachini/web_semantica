package main;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ModelMaker;
import com.hp.hpl.jena.vocabulary.XSD;


public class Ontologia {
	//Classe de manuten��o da Ontologia	
	
	//Construtor da classe
	public Ontologia(){		
	}
	
	//metodo para criar a ontologia
	public void modelarOntologia(){
		String dir = "/home/luan";
		String asno = "http://enderecoAcademicSocialNetworkOntology#";
		OntModel base = null;
		String foaf = "http://xmlns.com/foaf/0.1/";
		String aiiso = "http://pasno.org/vocab/aiiso/schema#";
		String aiiso_roles = "http://pasno.org/vocab/aiiso-roles/schema#";		
		
		
		try{
			
			//Criando o arquivo fisico
			ModelMaker maker = ModelFactory.createFileModelMaker(dir);
			Model modelo = maker.createModel("AcademicSocialNetworkOntology.owl",false);
			//Definindo linguagem de Ontologia
			OntModelSpec spec = new OntModelSpec(OntModelSpec.OWL_DL_MEM);
			spec.setImportModelMaker(maker);			
			base = ModelFactory.createOntologyModel(spec,modelo);
			
			//Iniciando desenvolvimento
			base.begin();
			
			//Classes
			OntClass Thing = base.createClass(asno + "Thing");
			
			OntClass Person = base.createClass(asno + "Person");						
			Person.setSuperClass(Thing);
			
			OntClass Document = base.createClass(asno + "Document");
			Document.setSuperClass(Thing);
			
			OntClass Institution = base.createClass(asno + "Institution");
			Institution.setSuperClass(Thing);
			
			OntClass College = base.createClass(asno + "College");
			College.setSuperClass(Institution);
			
			OntClass Course = base.createClass(asno + "Course");
			Course.setSuperClass(Thing);
			
			//Knowledge Grouping...
			//????
			OntClass GrandeArea = base.createClass(asno + "GrandeArea");			
			OntClass Area = base.createClass(asno + "Area");
			Area.setSuperClass(GrandeArea);
			
			OntClass SubArea = base.createClass(asno + "SubArea");
			SubArea.setSuperClass(Area);
			
			OntClass AreaEspecifica = base.createClass(asno + "AreaEspecifica");
			AreaEspecifica.setSuperClass(SubArea);
			//????
			//------------
			
			OntClass Subject = base.createClass(asno + "Subject");
			Subject.setSuperClass(AreaEspecifica);
			
			OntClass Student = base.createClass(asno + "Student");
			Student.setSuperClass(Person);
			
			OntClass Professor = base.createClass(asno + "Professor");
			Professor.setSuperClass(Person);
			
			OntClass Research = base.createClass(asno + "Research");
			Research.setSuperClass(Thing);
			
			OntClass Researcher = base.createClass(asno + "Researcher");
			Researcher.setSuperClass(Professor);
			
			OntClass Research_Leader = base.createClass(asno + "Research_Leader");
			Research_Leader.setSuperClass(Researcher);
			
			OntClass Research_Student = base.createClass(asno + "Research_Student");
			Research_Student.setSuperClass(Student);
			
			//Definindo equivalencias entre classes: 
			//asno e foaf
			Person.setEquivalentClass(base.getResource(foaf + "Person"));
			Document.setEquivalentClass(base.getResource(foaf + "Document"));
			//asno e aiiso
			Institution.setEquivalentClass(base.getResource(aiiso + "Institution"));
			College.setEquivalentClass(base.getResource(aiiso + "College"));
			Course.setEquivalentClass(base.getResource(aiiso + "Course"));
			GrandeArea.setEquivalentClass(base.getResource(aiiso + "KnowledgeGrouping"));
			Area.setEquivalentClass(base.getResource(aiiso + "KnowledgeGrouping"));
			SubArea.setEquivalentClass(base.getResource(aiiso + "KnowledgeGrouping"));
			AreaEspecifica.setEquivalentClass(base.getResource(aiiso + "KnowledgeGrouping"));
			Subject.setEquivalentClass(base.getResource(aiiso + "Subject"));
			//asno e aiiso-roles
			Professor.setEquivalentClass(base.getResource(aiiso_roles + "Professor"));
			Research.setEquivalentClass(base.getResource(aiiso_roles + "Research"));
			Researcher.setEquivalentClass(base.getResource(aiiso_roles + "Researcher"));
			Research_Leader.setEquivalentClass(base.getResource(aiiso_roles + "Research_Leader"));
			Research_Student.setEquivalentClass(base.getResource(aiiso_roles + "Research_Student"));
			
			
			//Properties, Domains and Ranges
			ObjectProperty publications = base.createObjectProperty(foaf + "publicatons");
			publications.setDomain(Person);
			publications.setRange(Document);
			
			DatatypeProperty firstName = base.createDatatypeProperty(asno + "firstName");
			firstName.setDomain(Person);
			firstName.setRange(XSD.Name);  //---------REVER-------------------
			firstName.setEquivalentProperty(base.getProperty(foaf + "firstName"));
			
			DatatypeProperty lastName = base.createDatatypeProperty(asno + "lastName");
			lastName.setDomain(Person);
			lastName.setRange(XSD.Name);
			lastName.setEquivalentProperty(base.getProperty(foaf + "lastName"));
			
			ObjectProperty primaryTopic = base.createObjectProperty(asno + "primaryTopic");
			primaryTopic.setDomain(Document);
			primaryTopic.setRange(Thing);
			primaryTopic.setEquivalentProperty(base.getProperty(foaf + "primaryTopic"));
			
			ObjectProperty topic = base.createObjectProperty(asno + "topic");
			topic.setDomain(Document);
			topic.setRange(Thing);
			topic.setEquivalentProperty(base.getProperty(foaf + "topic"));
			
			ObjectProperty graduatedIn = base.createObjectProperty(asno + "graduatedIn");
			graduatedIn.setDomain(Person);
			graduatedIn.setRange(Course);
			graduatedIn.setEquivalentProperty(base.getProperty(asno + "graduatedIn"));
			
			ObjectProperty graduatedAt = base.createObjectProperty(asno + "graduatedAt");
			graduatedAt.setDomain(Person);
			graduatedAt.setRange(Institution);
			
			ObjectProperty courseHasGraduated = base.createObjectProperty(asno + "courseHasGraduated");
			courseHasGraduated.setDomain(Course);
			courseHasGraduated.setRange(Person);
			
			ObjectProperty hasGraduated = base.createObjectProperty(asno +"hasGraduated");
			hasGraduated.setDomain(Institution);
			hasGraduated.setRange(Person);
			
			ObjectProperty knows = base.createObjectProperty(asno + "knows");
			knows.setDomain(Person);
			knows.setRange(Person);
			knows.setEquivalentProperty(base.getProperty(foaf + "knows"));
			
			ObjectProperty currentProject = base.createObjectProperty(asno + "currentProject");
			currentProject.setDomain(Person);
			currentProject.setRange(Thing);
			currentProject.setEquivalentProperty(base.getProperty(foaf + "currentProject"));
			
			ObjectProperty pastProject = base.createObjectProperty(asno + "pastProject");
			pastProject.setDomain(Person);
			pastProject.setRange(Thing);
			pastProject.setEquivalentProperty(base.getProperty(foaf + "pastProject"));
			
			ObjectProperty participateOn = base.createObjectProperty(asno + "participateOn");
			participateOn.setDomain(Thing);
			participateOn.setRange(Person);
			
			ObjectProperty participatedOn = base.createObjectProperty(asno + "participatedOn");
			participatedOn.setDomain(Thing);
			participatedOn.setRange(Person);
			
			ObjectProperty autor = base.createObjectProperty(asno + "autor");
			autor.setDomain(Document);
			autor.setRange(Person);
			
			DatatypeProperty title = base.createDatatypeProperty(asno + "title");
			title.setDomain(Document);
			title.setRange(XSD.Name);
			
			ObjectProperty isPrimaryTopic = base.createObjectProperty(asno + "isPrimaryTopic");
			isPrimaryTopic.setDomain(Thing);
			isPrimaryTopic.setRange(Document);
			
			ObjectProperty isTopic = base.createObjectProperty(asno + "isPrimaryTopic");
			isTopic.setDomain(Thing);
			isTopic.setRange(Document);
			
			ObjectProperty part_of = base.createObjectProperty(asno + "part_of");
			part_of.setDomain(College);
			part_of.setRange(Institution);
			part_of.setEquivalentProperty(base.getProperty(aiiso + "part_of"));
			
			ObjectProperty hasPart = base.createObjectProperty(asno + "hasPart");
			hasPart.setDomain(Institution);
			hasPart.setRange(College);
			
			DatatypeProperty institutionAcronym = base.createDatatypeProperty(asno + "institutionAcronym");
			institutionAcronym.setDomain(Institution);
			institutionAcronym.setRange(XSD.Name);
			
			DatatypeProperty institutionName = base.createDatatypeProperty(asno + "institutionName");
			institutionName.setDomain(Institution);
			institutionName.setRange(XSD.Name);
			
			ObjectProperty hasCourse = base.createObjectProperty(asno + "hasCourse");
			hasCourse.setDomain(College);
			hasCourse.setRange(Course);
						
			ObjectProperty isTaught = base.createObjectProperty(asno + "isTaught"); 
			isTaught.setDomain(Course);
			isTaught.setRange(College);
			
			ObjectProperty graduatedAtCollege = base.createObjectProperty(asno + "graduatedAtCollege");
			graduatedAtCollege.setDomain(Person);
			graduatedAtCollege.setRange(College);
			
			ObjectProperty collegeHasGraduated = base.createObjectProperty(asno + "collegeHasGraduated");
			collegeHasGraduated.setDomain(College);
			collegeHasGraduated.setRange(Person);
			
			DatatypeProperty city = base.createDatatypeProperty(asno + "city");
			city.setDomain(College);
			city.setRange(XSD.Name);
			
			DatatypeProperty state = base.createDatatypeProperty(asno + "state");
			state.setDomain(College);
			state.setRange(XSD.Name);
			
			ObjectProperty graduatingIn = base.createObjectProperty(asno + "graduatingIn");
			graduatingIn.setDomain(Student);
			graduatingIn.setRange(Course);
			
			ObjectProperty isGraduating = base.createObjectProperty(asno + "isGraduating");
			isGraduating.setDomain(Course);
			isGraduating.setRange(Student);
			
			DatatypeProperty courseAcronym = base.createDatatypeProperty(asno + "courseAcronym");
			courseAcronym.setDomain(Course);
			courseAcronym.setRange(XSD.Name);
			
			DatatypeProperty courseName = base.createDatatypeProperty(asno + "courseName");
			courseName.setDomain(Course);
			courseName.setRange(XSD.Name);
			
			ObjectProperty inKnowledgeGrouping = base.createObjectProperty(asno + "inKnowledgeGrouping");
			inKnowledgeGrouping.setDomain(Course);
			inKnowledgeGrouping.setRange(AreaEspecifica);
			
			ObjectProperty knowledgeGroupingHasCourse = base.createObjectProperty(asno + "knowledgeGroupingHasCourse");
			knowledgeGroupingHasCourse.setDomain(AreaEspecifica);
			knowledgeGroupingHasCourse.setRange(Course);
			
			ObjectProperty hasSubject = base.createObjectProperty(asno + "hasSubject");
			hasSubject.setDomain(Course);
			hasSubject.setRange(Subject);
			
			ObjectProperty isTaughtInCourse = base.createObjectProperty(asno + "isTaughtInCourse");
			isTaughtInCourse.setDomain(Subject);
			isTaughtInCourse.setRange(Course);
			
			ObjectProperty teaches = base.createObjectProperty(asno + "teaches");
			teaches.setDomain(Professor);
			teaches.setRange(Subject);
			
			ObjectProperty teachedBy = base.createObjectProperty(asno + "teachedBy");
			teachedBy.setDomain(Subject);
			teachedBy.setRange(Professor);
			
			ObjectProperty learns = base.createObjectProperty(asno + "learns");
			learns.setDomain(Student);
			learns.setRange(Subject);
			
			ObjectProperty learnedBy = base.createObjectProperty(asno + "learnedBy");
			learnedBy.setDomain(Subject);
			learnedBy.setRange(Student);
			
			DatatypeProperty subjectAcronym = base.createDatatypeProperty(asno + "subjectAcronym");
			subjectAcronym.setDomain(Subject);
			subjectAcronym.setRange(XSD.Name);
			
			DatatypeProperty subjectName = base.createDatatypeProperty(asno + "subjectName");
			subjectName.setDomain(Subject);
			subjectName.setRange(XSD.Name);
			
			DatatypeProperty code = base.createDatatypeProperty(asno + "code");
			code.setDomain(GrandeArea);
			code.setDomain(Area);
			code.setDomain(SubArea);
			code.setDomain(AreaEspecifica);
			code.setRange(XSD.Name);
			code.setEquivalentProperty(base.getProperty(aiiso + "code"));
			
			DatatypeProperty description = base.createDatatypeProperty(asno + "description");
			description.addDomain(GrandeArea);
			description.addDomain(Area);
			description.addDomain(SubArea);
			description.addDomain(AreaEspecifica);
			description.setRange(XSD.Name);
			description.setEquivalentProperty(base.getProperty(aiiso + "description"));
			
			
			ObjectProperty knowledgeGrouping = base.createObjectProperty(asno + "knowledgeGrouping");
			knowledgeGrouping.addDomain(Area);
			knowledgeGrouping.addDomain(SubArea);
			knowledgeGrouping.addDomain(AreaEspecifica);
			knowledgeGrouping.addRange(GrandeArea);
			knowledgeGrouping.addRange(Area);
			knowledgeGrouping.addRange(SubArea);
			knowledgeGrouping.setEquivalentProperty(base.getProperty(aiiso + "knowledgeGrouping"));
			
			ObjectProperty hasKnowledgeGrouping = base.createObjectProperty(asno + "hasKnowledgeGrouping");
			hasKnowledgeGrouping.addDomain(GrandeArea);
			hasKnowledgeGrouping.addDomain(Area);
			hasKnowledgeGrouping.addDomain(SubArea);
			hasKnowledgeGrouping.addRange(Area);
			hasKnowledgeGrouping.addRange(SubArea);
			hasKnowledgeGrouping.addRange(AreaEspecifica);
			
			DatatypeProperty researchTitle = base.createDatatypeProperty(asno + "researchTitle");
			researchTitle.setDomain(Research);
			researchTitle.setRange(XSD.Name);
			
			ObjectProperty researchField = base.createObjectProperty(asno + "researchField");
			researchField.setDomain(Research);
			researchField.setRange(AreaEspecifica);
			
			ObjectProperty researcher = base.createObjectProperty(asno + "researcher");
			researcher.setDomain(Research);
			researcher.setRange(Researcher);
			
			ObjectProperty leaderedBy = base.createObjectProperty(asno + "leaderedBy");
			leaderedBy.setDomain(Research);
			leaderedBy.setRange(Research_Leader);
			
			ObjectProperty researchStudent = base.createObjectProperty(asno + "researchStudent");
			researchStudent.setDomain(Research);
			researchStudent.setRange(Research_Student);
			
			ObjectProperty hasResearch = base.createObjectProperty(asno + "hasResearch");
			hasResearch.setDomain(AreaEspecifica);
			hasResearch.setRange(Research);
			
			ObjectProperty researches = base.createObjectProperty(asno + "researches");
			researches.setDomain(Researcher);
			researches.setRange(Research);
			
			ObjectProperty leads = base.createObjectProperty(asno + "leads");
			leads.setDomain(Research_Leader);
			leads.setRange(Research);
			
			ObjectProperty studentResearches = base.createObjectProperty(asno + "studentResearches");
			studentResearches.setDomain(Research_Student);
			studentResearches.setRange(Research);
			
			//Definicao dos relacionamentos entre as propriedades
			//Inversas
			publications.setInverseOf(autor);
			primaryTopic.setInverseOf(isPrimaryTopic);
			topic.setInverseOf(isTopic);
			graduatedIn.setInverseOf(courseHasGraduated);
			graduatedAt.setInverseOf(hasGraduated);
			currentProject.setInverseOf(participateOn);
			pastProject.setInverseOf(participatedOn);
			part_of.setInverseOf(hasPart);
			hasCourse.setInverseOf(isTaught);
			graduatedAtCollege.setInverseOf(collegeHasGraduated);
			graduatingIn.setInverseOf(isGraduating);
			inKnowledgeGrouping.setInverseOf(knowledgeGroupingHasCourse);
			hasSubject.setInverseOf(isTaughtInCourse);
			teaches.setInverseOf(teachedBy);
			learns.setInverseOf(learnedBy);
			knowledgeGrouping.setInverseOf(hasKnowledgeGrouping);
			researchField.setInverseOf(hasResearch);
			researcher.setInverseOf(researches);
			leaderedBy.setInverseOf(leads);
			researchStudent.setInverseOf(studentResearches);
			
			//Propriedade Simetrica
			knows.convertToSymmetricProperty();		
			
			//Encerrando edicao
			base.commit();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}//Class
