package semantica;

import java.util.Arrays;
import java.util.List;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

import dijkstra.Grafo;
import dijkstra.No;
/**
 * Classe que contem os metodos para realiza��o da an�lise de similaridade entre dados
 * @author Luan Fakelmann
 *
 */
public class Similaridade {
	
	public Similaridade(Grafo ontologia, Resource[] conceitos){
		this.gr = ontologia;
		this.conceitos = conceitos;
	}
		
	/**
	 * Metodo do Menor Caminho - Bouquet
	 * Calcula a similaridade atravez do menor caminho.
	 * @param normalizado - Vetor normalizado de distancias entre os conceitos
	 * @return Matriz contendo a similaridade entre todos os conceitos
	 */
	public float[][] similaridadeMenorCaminho(float[][] normalizado){
		int size = normalizado.length;
		float[][] similar = new float[size][size];
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
				similar[i][j] = 1- normalizado[i][j];
			}
		}
		return similar;
	}
	
	/**
	 * Metodo Cosseno entre Vetores
	 * Calcula a similaridade entre os conceitos pela analise do cosseno entre os vetores
	 * @param normalizado - Vetor normalizado de distancias entre os conceitos 
	 * @return Matriz contendo a similaridade entre todos os conceitos
	 */
	public float[][] similaridadeCos(float[][] normalizado){
		int i, j, k;
		int size = normalizado.length;
		float sim[][] = new float[size][size];
		for(i=0; i<size; i++){
			float u[] = new float[size];
			float v[] = new float[size];
			for(j=0; j<size; j++){
				u[j] = normalizado[i][j];				
			}
			for(j=0; j<size; j++){
				for(k=0; k<size; k++){
					v[k] = normalizado[j][k];
				}
				sim[i][j] = similaridadeCos(u, v);
			}
		}
		return sim;
	}
		
	
	/**
	 * Metodo do Menor Caminho Escalado - Leacock and Chodorow
	 * @param conceitos - Vetor contendo os conceitos a serem utilizados
	 * @param distancias - Matriz contendo as distancias entre os conceitos
	 * @return Matriz contendo a similaridade entre todos os conceitos
	 */
	public float[][] similaridadeMenorCamEsc(int[][] distancias){
		int size = conceitos.length;
		float[][] escalado = new float[size][size];
		float maiorDist = 0;
		boolean thing = false;
		int j,i=0;
		//Verificando se o conceito eh a raiz
		while(!thing){
			if(conceitos[i].getLocalName().equals("SpatialThing"))
				thing = true;
			else 
				i++;
		}
		//Obtendo profundidade da ontologia
		for(j=0; j<size; j++){
			if(distancias[i][j]>maiorDist){
				maiorDist = distancias[i][j];
			}
		}
		
		//Calculando menor caminho escalado
		for(i=0; i<size; i++){
			for(j=0; j<size; j++){
				escalado[i][j] = (float) (-1*( Math.log10( (distancias[i][j]/(2*maiorDist)) ) ));
				if(Float.isInfinite(escalado[i][j])){
					escalado[i][j]=1;
				}
			}
		}
		return escalado;
	}
	
	/**
	 * Metodo da Profundidade - Wu
	 * @param gr - Grafo representando a ontologia
	 * @param conceitos - Vetor contendo os conceitos a serem utilizados
	 * @param propriedade - propriedade a ser analisada como aresta
	 * @return Matriz contendo a similaridade entre todos os conceitos
	 */
	public float[][] similaridadeProfundidade(Property propriedade){
		int profA=0, profB=0, profComum=0;
		int a=0,b=0;
		float[][] profundidade;
		//Obtendo menores caminhos entre os a raiz e os conceitos
		List<No[]> caminhos = gr.menorCaminhoRaiz(conceitos, propriedade);
		gr.setDistanciasRoot(caminhos);
		for(No[] path: gr.getDistanciasRoot())
			System.out.println(Arrays.deepToString(path));
		profundidade = new float[conceitos.length][conceitos.length];
		
		//Obtendo caminhos e profundidades
		for(No[] camA : caminhos){
			profA = camA.length-1;
			for(No[] camB : caminhos){
				profB = camB.length-1;
				boolean comumOk = false;				
				//buscando profundidade do conceito em comum
				profComum = especificidadeComum(camA, camB);	
				float somaProf = profA + profB;
				if(somaProf==0)
					profundidade[a][b]= 1;
				else
					profundidade[a][b] = (2*profComum)/somaProf;
				b++;
			}
			b=0;
			a++;
		}	
		return profundidade;
	}
	
	/**
	 * Metodo por Menor Caminho e Especificidade Comum - Nguyer
	 * @param co - Instancia do objeto que representa o grafo (Classe Grafo)
	 * @param distancias - vetor contendo as distancias entre os conceitos
	 * @param conceitos - Vetor contendo os conceitos a serem utilizados
	 * @param propriedade - propriedade a ser analisada como aresta
	 * @return Matriz contendo a similaridade entre todos os conceitos
	 */
	public float[][] similaridadeCSpec(int[][] distancias, Property propriedade){
		VetorConceitual vc = new VetorConceitual();
		int size = conceitos.length;
		int i=0, j=0;
		float maiorDist = 0;
		float[][] caminhosComuns = new float[size][size];
		float[][] simCSpec = new float[size][size];
		boolean thing = false;
		
		int alfa=2, beta=3, k=1;
		
		
		//Convertendo distancias (de contagem por arestas para contagem por nos)
		for(i=0; i<size; i++){
			for(j=0; j<size; j++)
				distancias[i][j] ++;
		}
		
		//Obtendo caminhos dos conceitos ate a raiz
		List<No[]> caminhos = gr.getDistanciasRoot();
		//Buscando conceito raiz
		i=0;
		while(!thing){
			if(conceitos[i].getLocalName().equals("SpatialThing"))
				thing = true;
			else 
				i++;
		}
		//Obtendo profundidade da ontologia
		for(j=0; j<size; j++){
			if(distancias[i][j]>maiorDist){
				maiorDist = distancias[i][j];
			}
		}
		i=0;
		j=0;
		//Obtendo especificidades conmuns entre os conceitos
		for(No[] caminhoA : caminhos){
			for(No[] caminhoB : caminhos){
				caminhosComuns[i][j] = especificidadeComum(caminhoA, caminhoB);
				//Analisando em qual regra se encaixa
				
				//Se ambos os conceitos pertencem ao primary cluster
				if((caminhoA[0].getCluster()).equals(caminhoB[0].getCluster()) && (caminhoA[0].getCluster()).equals("people")){
					simCSpec[i][j] = (float) Math.log10((Math.pow((distancias[i][j] -1), alfa) * (Math.pow((maiorDist - caminhosComuns[i][j]) ,beta)) + k));
				}
				//Se um conceito pretence ao primary cluster e outro n�o
				else if(!(caminhoA[0].getCluster()).equals(caminhoB[0].getCluster()) && 
						((caminhoA[0].getCluster()).equals("people") || (caminhoB[0].getCluster()).equals("people"))){
					//
					//
					//
					//PAREI AQUI
					//
					//
					//
					//
				}
				i++;
			}
			i=0;
			j++;
		}

		simCSpec= vc.normalizarVetor(simCSpec);
		for(i=0; i<size; i++){
			for(j=0; j<size; j++){
				simCSpec[i][j] = 1-simCSpec[i][j];
			}
		}
		return simCSpec;
		
	}

	
	/**
	 * Metodo para calcular o coeficiente de correlacao dos dados
	 * @param metodoA - matriz contendo a similaridade dos dados por meio de um determinado metodo de similaridade
	 * @param metodoB - matriz contendo a similaridade dos dados por meio de outro determinado metodo de similaridade
	 * @return fator de correlacao
	 */
	public float correlacao(float[][] metodoA, float[][] metodoB){
		int size = metodoA.length;
		int n = size * size;
		float somaA = 0;
		float somaB = 0;
		float somaAB = 0;
		float somaAquad = 0;
		float somaBquad =0;
		float r;
		
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
				somaA += metodoA[i][j];
				somaB += metodoB[i][j];
				somaAquad += Math.pow(metodoA[i][j], 2);
				somaBquad += Math.pow(metodoB[i][j], 2);
				somaAB += metodoA[i][j] * metodoB[i][j];
			}
		}
		r = (float) ((n*somaAB - (somaA*somaB)) / (Math.sqrt(n*somaAquad - Math.pow(somaA,2)) * Math.sqrt(n*somaBquad - Math.pow(somaB,2))));
		return r;		
	}
	
	//Metodo para calcular a similaridade entre dois vetores pelo cosseno entre os mesmos
	private float similaridadeCos(float[] u, float[] v){
		float prod = 0;
		float modU = 0;
		float modV = 0;
		for(int i=0; i<u.length; i++){
			prod += u[i]*v[i];
			modU += Math.pow(u[i], 2);
			modV += Math.pow(v[i], 2);
		}
		modU = (float) Math.sqrt(modV);
		modV = (float) Math.sqrt(modV);
		return prod/(modU*modV);
	}
	
	//Profundidade do primeiro conceito em comum entre os caminhos de dois conceitos a raiz
	private int especificidadeComum(No[] caminhoA, No[] caminhoB){
		boolean comumOk = false;
		int profComum = 0;
		for(int i=0; i<caminhoA.length; i++){					
			for(int j=0; j<caminhoB.length; j++){
				if((caminhoA[i].getConceito()).equals(caminhoB[j].getConceito()) && !comumOk){							
					profComum = caminhoA.length - i -1;
					comumOk = true;
				}
			}			
		}
		return profComum;
	}
	
	private Grafo gr;
	private Resource[] conceitos;
}
	
