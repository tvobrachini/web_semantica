package semantica;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

import RDF.BaseRDF;

public class VetorConceitual extends BaseRDF {
	
	public VetorConceitual()
	{/*Construtor*/}
	
	//Metodo para normalizar os vetores
	private float[] normalizarVetor(float[] vetor){
		int soma = 0;
		float v, normalizado[]= new float[vetor.length];
		for(int i=0; i<vetor.length; i++){
			soma += Math.pow(vetor[i],2);
		}
		v = (float) Math.sqrt(soma);
		for(int i=0; i<vetor.length; i++){
			normalizado[i] = vetor[i]/v;
		}
		return normalizado;
	}
	
	//Metodo para normalizar os vetores de uma matriz
	public float[][] normalizarVetor(int[][] matriz){
		int size = matriz.length;
		float normalizado[][] = new float[size][size];
		for(int i=0; i<size; i++){
			float aux[] = new float[size];
			for(int j=0; j<size; j++){
				aux[j] = matriz[i][j];
			}
			aux = normalizarVetor(aux);
			for(int j=0; j<size; j++){
				normalizado[i][j] = aux[j];
			}
		}
		return normalizado;
	}
	
	//Metodo para normalizar os vetores de uma matriz
		public float[][] normalizarVetor(float[][] matriz){
			int size = matriz.length;
			float normalizado[][] = new float[size][size];
			for(int i=0; i<size; i++){
				float aux[] = new float[size];
				for(int j=0; j<size; j++){
					aux[j] = matriz[i][j];
				}
				aux = normalizarVetor(aux);
				for(int j=0; j<size; j++){
					normalizado[i][j] = aux[j];
				}
			}
			return normalizado;
		}
	
	//Metodo para carregar um conceito
	public Resource carregarConceito(String ns, String conceito){
		abrirAsnOnt();
		return modelo.getResource(ns+conceito);
	}
	
	public int profundidade(Resource conceito, Property propriedade){
		int prof = analise(conceito, propriedade);
		if(prof==0) return 1;
		else return prof;
	}
	
	//Metodo para analiasar a profundidade de determinado conceito na Ontologia
	public int analise(Resource conceito, Property propriedade){		
		NodeIterator objeto = null;
		Model base = null;
		base = conceito.getModel();
		//Verificando se o conceito possui a propriedade
		if(conceito.hasProperty(propriedade)){
			System.out.println(conceito.toString() + " - subClass of ");
			objeto = base.listObjectsOfProperty(conceito, propriedade);			
			
			//Analisando objetos recuperados a partir da propriedade
			while(objeto.hasNext()){
				conceito = (Resource) objeto.next();
			}			
			return 1 + analise(conceito, propriedade);
		}
		else{
			System.out.println("Thing");
			return 0;
		}
	}
	
	//metodo para listar a profundidade de todos os conceitos
	//da asnOnt
	public void listarProfundidadeConceitos(){
		Resource[] conceito = getTodosConceitos();
		for(int i=0; i<conceito.length;i++){
			int prof = profundidade(conceito[i], subClassOf);
			System.out.println(conceito[i].getLocalName()+" - Profundidade: "+prof);
			System.out.println();
		}
	}
	
}//Class
